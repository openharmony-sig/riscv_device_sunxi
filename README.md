# OpenHarmony 构建 d1_nezha

## 概述

【哪吒】是全志在线基于全志科技D1芯片定制的AIoT开发板，是全球首款支持64bit RISC-V指令集并支持Linux系统的可量产开发板。

本文介绍如何在OpenHarmony平台上构建D1固件。

版本说明： 

- 当前仅支持D1内核与最小rootfs的构建、D1固件打包。

后续开发计划：

- 添加wayland支持。
- 添加基础服务支持。
- ...

## 源码获取

按照下面方法获取源码。由于OpenHarmony更新较频繁，为保证稳定，在sunxi_d1.xml中对各仓库指定commit下载。

```
repo init -u https://gitee.com/openharmony-sig/manifest.git -b master -m sunxi_d1.xml --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

源码仓库主要有四个来源：

1.  OpenHarmony官方仓库 - https://gitee.com/openharmony
2.  OpenHarmony-sig仓库 - https://gitee.com/openharmony-sig
3.  iscas-taiyang支持riscv的仓库 - https://gitee.com/iscas-taiyang
4.  D1平台相关仓库 - https://gitee.com/allwinnertech-d1


## 构建方法

当前仅支持在OpenHarmony上构建D1内核与最小rootfs，需要借助D1的Tina Linux环境中的内核linux-4.9。

### Tina Linux D1环境下载

参考 https://d1.docs.aw-ol.com/ 下载D1的Tina Linux SDK

### OpenHarmony D1 构建

假定tina-d1为D1 Tina Linux环境根目录；假定OpenHarmony为D1 OpenHarmony根目录。

- 将tina-d1/lichee/linux-5.4复制到OpenHarmony/kernel/linux-5.4
- cd OpenHarmony，运行./build/prebuilts_download.sh，下载相关编译工具链与host端工具
- 将tina-d1/prebuilt/gcc/linux-x86/riscv/toolchain-thead-glibc/riscv64-glibc-gcc-thead_20200702复制到OpenHarmony/prebuilts/gcc/linux-x86/riscv/riscv64-glibc-gcc-thead_20200702
- 从 https://pan.baidu.com/s/1x1U11oIHCrTPTuQTQjpBDw 提取码: qchw 下载llvm-riscv工具链llvm-riscv-0918.tar.gz（临时使用），解压到OpenHarmony/prebuilts/clang/ohos/linux-x86_64/llvm-riscv/
- ./build.sh --product-name sunxi_d1 --ccache

至此，OpenHarmony环境下的D1内核与最近rootfs构建完成，成功的log如下所示。

```
c targets overlap rate statistics
subsystem               files NO.       percentage      builds NO.      percentage      overlap rate
hiviewdfx                     15        0.6%          15        0.6%    1.00
startup                       37        1.5%          37        1.5%    1.00
third_party                  936        38.7%        936        38.7%   1.00
utils                         97        4.0%          97        4.0%    1.00

c overall build overlap rate: 1.00


post_process
=====build sunxi_d1 successful.
2021-09-26 19:35:09
++++++++++++++++++++++++++++++++++++++++

```

### 打包sunxi_d1固件

在SDK根目录执行: ./device/sunxi/build/pack, 生成out/d1-nezha/tina_d1-nezha_uart0.img文件，将此文件烧录到D1开发板即可。
